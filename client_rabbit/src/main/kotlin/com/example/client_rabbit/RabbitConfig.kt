package com.example.client_rabbit

import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@EnableRabbit
@Configuration
class RabbitConfig {

    @Bean
    fun connectionFactory() : ConnectionFactory =
            CachingConnectionFactory("localhost")

    @Bean
    fun rabbitAdmin() : RabbitAdmin =
            RabbitAdmin(connectionFactory())

    @Bean
    fun rabbitTemplate() : RabbitTemplate =
            RabbitTemplate(connectionFactory())

    @Bean
    fun queue() : Queue =
            Queue("queue")
}