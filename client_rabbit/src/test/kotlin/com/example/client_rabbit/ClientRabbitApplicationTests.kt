package com.example.client_rabbit

import org.junit.jupiter.api.Test
import org.springframework.amqp.core.AmqpTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ClientRabbitApplicationTests {

    @Autowired
    private lateinit var amqpTemplate: AmqpTemplate

    @Test
    internal fun callServerServiceWithRabbit() {
        amqpTemplate.convertAndSend("queue", "Hello world")
    }

}