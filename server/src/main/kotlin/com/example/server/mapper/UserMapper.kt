package com.example.server.mapper

import com.example.server.data.User
import com.example.server.data.UserDto
import org.springframework.stereotype.Component

@Component
class UserMapper {

    fun userToUserDto(u: User) : UserDto = with(u) {
        UserDto(
                name = "$first_name $last_name",
                address = "${address?.country}, ${address?.city}, ${address?.address}"
        )
    }

}