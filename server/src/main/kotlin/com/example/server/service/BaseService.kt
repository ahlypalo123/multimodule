package com.example.server.service

import com.example.server.data.User
import com.example.server.mapper.UserMapper
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Service
import java.io.StringReader
import javax.xml.bind.JAXBContext
import javax.xml.bind.JAXBException
import kotlin.jvm.Throws

@Service
class BaseService(
        private val userMapper: UserMapper,
        private val userContext: JAXBContext,
        private val jsonMapper: ObjectMapper
) {

    @Throws(JAXBException::class)
    fun convertToJson(body: String) : String {
        val user = userContext.createUnmarshaller().unmarshal(StringReader(body)) as User
        val dto = userMapper.userToUserDto(user)
        return jsonMapper.writeValueAsString(dto)
    }

}