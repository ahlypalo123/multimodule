package com.example.server.config

import com.example.server.service.BaseService
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import javax.xml.bind.JAXBException

@EnableRabbit
@Component
class RabbitListener(private val baseService: BaseService) {

    companion object {
        private val logger = LoggerFactory.getLogger("RabbitListener")
    }

    @RabbitListener(queues = [ "queue" ])
    fun processQueue(message: String) {
        try {
            val json = baseService.convertToJson(message)
            println("xml: $message")
            println("converted to json: $json")
        } catch (ex: JAXBException) {
            logger.info("error converting to json: $message")
        }
    }
}