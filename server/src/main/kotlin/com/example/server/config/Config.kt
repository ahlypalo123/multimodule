package com.example.server.config

import com.example.server.data.User
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.xml.bind.JAXBContext

@Configuration
class Config {

    @Bean
    fun userJaxbXmlContext() : JAXBContext =
        JAXBContext.newInstance(User::class.java)

    @Bean
    fun jsonMapper() : ObjectMapper =
        ObjectMapper()

}