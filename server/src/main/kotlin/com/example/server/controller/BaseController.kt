package com.example.server.controller

import com.example.server.service.BaseService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class BaseController(
    private val baseService: BaseService
) {

    @PostMapping("/convert-to-json", consumes = ["application/xml"],
            produces = ["application/json"])
    fun convertToJson(@RequestBody body: String) : String =
        baseService.convertToJson(body)

}