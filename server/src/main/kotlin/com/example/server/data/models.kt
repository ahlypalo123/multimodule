package com.example.server.data

import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@XmlRootElement(name = "User")
@XmlType(propOrder = ["first_name", "last_name", "address"])
data class User(
        var first_name: String = "",
        var last_name: String = "",
        var address: Address? = null
)

data class UserDto(
        val name: String = "",
        val address: String = ""
)

data class Address(
        var country: String = "",
        var city: String = "",
        var address: String = "",
)