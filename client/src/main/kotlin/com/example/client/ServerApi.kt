package com.example.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@FeignClient(value = "localhost", url = "http://localhost:8081/")
interface ServerApi {
    @PostMapping(value = ["/convert-to-json"], consumes = ["application/xml"],
            produces = ["application/json"])
    fun convertToJson(@RequestBody body: String): String
}