package com.example.client

import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest


@SpringBootTest
class ClientServiceApplicationTests {

	companion object {
		private val logger = LoggerFactory.getLogger("ClientServiceApplicationTests")
	}

	@Autowired
	private lateinit var api: ServerApi

	@Test
	internal fun callServerServiceWIthFeign() {
		val xml = "<User>\n" +
				"    <first_name>Andrey</first_name>\n" +
				"    <last_name>HPL</last_name>\n" +
				"    <address>\n" +
				"      <country>Russia</country>\n" +
				"      <city>Rostov</city>\n" +
				"      <address>Sokolova</address>\n" +
				"    </address>\n" +
				"</User>"
		val json = api.convertToJson(xml)
		logger.info(json)
	}
}
